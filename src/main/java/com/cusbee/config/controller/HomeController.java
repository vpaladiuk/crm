package com.cusbee.config.controller;


import com.cusbee.config.dto.AccountDetailtUpdateDto;
import com.cusbee.config.dto.AccountDto;
import com.cusbee.config.entity.Account;
import com.cusbee.config.entity.enums.CrudOperation;
import com.cusbee.config.exception.ApplicationException;
import com.cusbee.config.exception.BaseException;
import com.cusbee.config.repository.AccountRepository;
import com.cusbee.config.service.AccountService;
import com.cusbee.config.utils.ErrorCodes;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@RestController
@RequestMapping(value = "account")
public class HomeController {

    @Autowired
    private AccountRepository accountRepository;

    @Autowired
    private AccountService accountService;

    @RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<Account> saveAccount(@RequestBody AccountDto accountDto) throws BaseException{
        Account account = accountService.save(accountDto, CrudOperation.CREATE);
        return new ResponseEntity<Account>(account, HttpStatus.CREATED);
    }

    @RequestMapping(value = "getAll", method = RequestMethod.GET)
    public List<Account> getAll() {
        return accountRepository.getAll();
    }

    @RequestMapping(value = "remove/{id}", method = RequestMethod.POST)
    public ResponseEntity<String> removeAccount(@PathVariable("id") Long id){
        Account account = accountRepository.getOne(id);
        accountRepository.delete(account);
        return new ResponseEntity<String>(HttpStatus.OK);
    }

    @RequestMapping(value = "/updateAccountDetailsByAccountId")
    public ResponseEntity<Account> updateAccountDetails(@RequestBody AccountDetailtUpdateDto accountDto){
        Account account = accountService.updateAccountDetails(accountDto);
        return new ResponseEntity<Account>(HttpStatus.CREATED);
    }
}
