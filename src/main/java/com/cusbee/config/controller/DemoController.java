package com.cusbee.config.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class DemoController {

    @RequestMapping(value = "demo_Method", method = RequestMethod.GET)
    public String demo(){
        return null;
    }
}
