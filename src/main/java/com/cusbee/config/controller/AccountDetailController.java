package com.cusbee.config.controller;

import com.cusbee.config.dto.AccountDetailDto;
import com.cusbee.config.entity.AccountDetail;
import com.cusbee.config.entity.enums.CrudOperation;
import com.cusbee.config.exception.BaseException;
import com.cusbee.config.repository.AccountDetailRepository;
import com.cusbee.config.service.AccountDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping(value = "accountDetail")
public class AccountDetailController {

    @Autowired
    private AccountDetailRepository accountDetailRepository;

    @Autowired
    private AccountDetailService accountDetailService;

    @RequestMapping(value = "save", method = RequestMethod.POST)
    public ResponseEntity<AccountDetail> saveAccountDetail(@RequestBody AccountDetailDto accountDetailDto) throws BaseException {
        AccountDetail accountDetail = accountDetailService.saveAccountDetail(accountDetailDto, CrudOperation.CREATE);
        return new ResponseEntity<AccountDetail>(accountDetail, HttpStatus.CREATED);
    }

    @RequestMapping(value = "getAll", method = RequestMethod.GET)
    public List<AccountDetail> getAll() {
        return accountDetailRepository.getAll();
    }

}
