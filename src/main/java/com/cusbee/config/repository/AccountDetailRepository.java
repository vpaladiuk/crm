package com.cusbee.config.repository;


import com.cusbee.config.entity.AccountDetail;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AccountDetailRepository extends JpaRepository<AccountDetail, Long>{

    @Query(value = "select * from account_detail", nativeQuery = true)
    List<AccountDetail> getAll();

}
