package com.cusbee.config.dto;

import com.cusbee.config.entity.Account;
import com.cusbee.config.entity.Project;
import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class AccountDetailDto {

    private Long id;

   // private String imagePath;

    private String firstName;

    private String lastName;

    private String email;

  //  private Account account;

   // private List<Project> projects;

}
