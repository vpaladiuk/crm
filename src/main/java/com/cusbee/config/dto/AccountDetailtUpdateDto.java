package com.cusbee.config.dto;

import com.cusbee.config.entity.AccountDetail;
import lombok.Getter;
import lombok.Setter;

/**
 * Created by Asus on 14.12.2016.
 */

@Getter
@Setter
public class AccountDetailtUpdateDto {

    public Long accountId;

    public AccountDetail accountDetail;
}
