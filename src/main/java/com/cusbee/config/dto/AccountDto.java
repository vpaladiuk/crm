package com.cusbee.config.dto;

import com.cusbee.config.entity.AccountDetail;
import com.cusbee.config.entity.enums.AccountType;
import com.cusbee.config.entity.enums.DepartmentType;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.constraints.Size;

@Getter
@Setter
public class AccountDto {

    private Long id;

    @NotEmpty(message = "Enter your name, please")
    private String username;

    @NotEmpty(message = "Enter your password, please")
    @Size(min = 4, max = 20, message = "Your password must be between 4 and 20 characters")
    private String password;

    @NotEmpty(message = "Enter your password again, please")
    private String confirmPassword;

    @NotEmpty(message = "Enter your position, please")
    private String position;

    private DepartmentType departmentType;

    private AccountType accountType;

    private AccountDetail accountDetail;

    private boolean isEnable;
}
