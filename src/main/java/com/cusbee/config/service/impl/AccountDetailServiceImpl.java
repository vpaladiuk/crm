package com.cusbee.config.service.impl;


import com.cusbee.config.dto.AccountDetailDto;
import com.cusbee.config.entity.AccountDetail;
import com.cusbee.config.entity.enums.CrudOperation;
import com.cusbee.config.exception.ApplicationException;
import com.cusbee.config.repository.AccountDetailRepository;
import com.cusbee.config.service.AccountDetailService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class AccountDetailServiceImpl implements AccountDetailService{

    @Autowired
    private AccountDetailRepository accountDetailRepository;

    @Override
    public AccountDetail saveAccountDetail(AccountDetailDto accountDetailDto, CrudOperation crudOperation) {
        AccountDetail accountDetail;
        switch (crudOperation) {
            case CREATE:
                accountDetail = createAccountDetail(accountDetailDto);
                break;
            default:
                throw new ApplicationException("Not valid operation");
        }

        return accountDetail;
    }

    private AccountDetail createAccountDetail(AccountDetailDto accountDetailDto) {

        AccountDetail accountDetail = AccountDetail.Build()
                        .setFirstName(accountDetailDto.getFirstName())
                        .setLastName(accountDetailDto.getLastName())
                        .setEmail(accountDetailDto.getEmail())
                        .build();

        return accountDetailRepository.save(accountDetail);
    }

    @Override
    public AccountDetail getAccountDetail(Long id) {
        return null;
    }
}
