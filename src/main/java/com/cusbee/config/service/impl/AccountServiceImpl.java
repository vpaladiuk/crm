package com.cusbee.config.service.impl;

import com.cusbee.config.dto.AccountDetailtUpdateDto;
import com.cusbee.config.dto.AccountDto;
import com.cusbee.config.entity.Account;
import com.cusbee.config.entity.AccountDetail;
import com.cusbee.config.entity.enums.CrudOperation;
import com.cusbee.config.exception.ApplicationException;
import com.cusbee.config.repository.AccountRepository;
import com.cusbee.config.service.AccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;

@Service
public class AccountServiceImpl implements UserDetailsService, AccountService {

    @Autowired
    private AccountRepository accountRepository;

    /*@Autowired
    private BCryptPasswordEncoder bCryptPasswordEncoder;*/

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Account account = accountRepository.findByUsername(username);
        if(Objects.isNull(account)) {
            throw new UsernameNotFoundException("Username: " + username + " not found");
        }
        return account;
    }

    @Override
    public Account save(AccountDto accountDto, CrudOperation crudOperation) {
        Account account;
        switch (crudOperation) {
            case CREATE:
               // AccountDetail accountDetail = accountDto.getAccountDetail();
               // AccountDetail accountDetailSaved = saveAccountDetail(accountDetail);
                account = createAccount(accountDto);
              //  account.setAccountDetail(accountDetailSaved);
                break;
            case UPDATE:
                account = updateAccount(accountDto);
                break;
            default:
                throw new ApplicationException("Not valid operation");
        }
        return account;
    }

    private AccountDetail saveAccountDetail(AccountDetail accountDetail) {
       // AccountDetail accountDetail1
        return null;
    }

    private Account updateAccount(AccountDto accountDto) {

       Account account = accountRepository.getOne(accountDto.getId());

        account = Account.Build()
                .setUsername(accountDto.getUsername())
                .setPassword(accountDto.getPassword())
                .setPosition(accountDto.getPosition())
                .setAccountType(accountDto.getAccountType())
                .setDepartment(accountDto.getDepartmentType())
                .setAccountDetail(accountDto.getAccountDetail())
                .build();

        return accountRepository.save(account);
    }

    private Account createAccount(AccountDto accountDto) {

        Account account = Account.Build()
                .setUsername(accountDto.getUsername())
                .setPassword(/*bCryptPasswordEncoder.encode*/(accountDto.getPassword()))
                .setPosition(accountDto.getPosition())
                .setDepartment(accountDto.getDepartmentType())
                .build();

        return accountRepository.save(account);
    }

    @Override
    public List<Account> getAll() {
        return accountRepository.getAll();
    }

    @Override
    public Account getAccount(Long id) {
        return accountRepository.getOne(id);
    }

    @Override
    public Account blockById(Long id) {
        Account account = accountRepository.getOne(id);
        account.setEnable(false);
        return account;
    }

    @Override
    public Account unblockById(Long id) {
        Account account = accountRepository.getOne(id);
        account.setEnable(true);
        return account;
    }

    public Account updateAccountDetails(AccountDetailtUpdateDto dto){
        Account account = getAccount(dto.getAccountId());
        account.getAccountDetail().setFirstName(dto.getAccountDetail().getFirstName());
        accountRepository.save(account);
        return account;
    }

}