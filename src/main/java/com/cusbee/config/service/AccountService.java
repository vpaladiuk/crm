package com.cusbee.config.service;

import com.cusbee.config.dto.AccountDetailtUpdateDto;
import com.cusbee.config.dto.AccountDto;
import com.cusbee.config.entity.Account;
import com.cusbee.config.entity.enums.CrudOperation;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Transactional
public interface AccountService {

    Account save(AccountDto accountDto, CrudOperation crudOperation);

    List<Account> getAll();

    Account getAccount(Long id);

    Account blockById(Long id);

    Account unblockById(Long id);

    Account updateAccountDetails(AccountDetailtUpdateDto dto);

}
