package com.cusbee.config.service;

import com.cusbee.config.dto.AccountDetailDto;
import com.cusbee.config.entity.AccountDetail;
import com.cusbee.config.entity.enums.CrudOperation;
import org.springframework.transaction.annotation.Transactional;


public interface AccountDetailService {

    AccountDetail saveAccountDetail(AccountDetailDto accountDetailDto, CrudOperation crudOperation);

    AccountDetail getAccountDetail(Long id);

}
