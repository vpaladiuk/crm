package com.cusbee.config.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Fetch;
import org.hibernate.annotations.FetchMode;

import javax.persistence.*;
import javax.validation.constraints.Pattern;
import java.util.List;

@Getter
@Setter
@Entity
@Table(name = "account_detail")
public class AccountDetail {

    private static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    @Id
    @GeneratedValue
    private Long id;

    @JsonIgnore
    @Column(name="imagepath")
    private String imagePath;

    @Column(name = "firstName", length = 50)
    @Pattern(regexp = "^[A-Z][a-z]+$", message = "Not valid. Ex: Tilman")
    private String firstName;

    @Column(name = "lastName", length = 50)
    @Pattern(regexp = "^[A-Z][a-z]+$", message = "Not valid. Ex: Schweiger")
    private String lastName;

    @Column(name = "email", length = 50)
    @Pattern(regexp = EMAIL_PATTERN, message = "Enter email in correct format, please")
    private String email;

    @JsonIgnore
    @OneToOne(fetch = FetchType.LAZY)
    private Account account;

    @JsonIgnore
    @ManyToMany(fetch = FetchType.LAZY)
    @JoinTable(name = "account_project",
            joinColumns = {@JoinColumn(name = "account_id")},
            inverseJoinColumns = {@JoinColumn(name = "project_id")})
    private List<Project> projects;

    public static Builder Build() {
        return new AccountDetail().new Builder();
    }

    public class Builder {

        public Builder setFirstName(String firstName) {
            AccountDetail.this.setFirstName(firstName);
            return this;
        }

        public Builder setLastName(String lastName) {
            AccountDetail.this.setLastName(lastName);
            return this;
        }

        public Builder setEmail(String email) {
            AccountDetail.this.setEmail(email);
            return this;
        }

        public AccountDetail build() {
            return AccountDetail.this;
        }
    }

}
