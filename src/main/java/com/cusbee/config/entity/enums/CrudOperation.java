package com.cusbee.config.entity.enums;


public enum CrudOperation {

    CREATE, READ, UPDATE, DELETE

}
