package com.cusbee.config.entity.enums;

public enum DepartmentType {

    FRONT_END, BACK_END, TESTING, DESIGN, STAFF, DEFAULT

}
