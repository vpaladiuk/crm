package com.cusbee.config.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDate;
import java.util.List;

@Getter
@Setter
@Table(name = "project")
@Entity
public class Project {

    @Id
    @GeneratedValue
    private Long id;

    @Column
    private String name;

    @Column
    private LocalDate startDate;

    @Column
    private LocalDate endDate;

    @ManyToMany(mappedBy = "projects")
    List<AccountDetail> accountDetails;

    @OneToMany(mappedBy = "project")
    List<Task> tasks;

}
