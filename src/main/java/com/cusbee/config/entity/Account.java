package com.cusbee.config.entity;

import com.cusbee.config.entity.enums.AccountType;
import com.cusbee.config.entity.enums.DepartmentType;
import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.FetchMode;
import org.hibernate.annotations.Fetch;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import javax.persistence.*;
import java.util.Collection;

@Entity
@Table(name = "account")
@Getter
@Setter
public class Account implements UserDetails {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "username", length = 50, unique = true, nullable = false)
    private String username;

    @Column(name = "password", length = 35, nullable = false)
    private String password;

    @Column(name = "position", length = 50)
    private String position;

    @Column(name = "accountType")
    @Enumerated(EnumType.STRING)
    private AccountType accountType;

    @Column(name = "departmentType")
    @Enumerated(EnumType.STRING)
    private DepartmentType departmentType;

    @Column(name = "isEnable")
    private boolean isEnable;

    @OneToOne(cascade = CascadeType.ALL, mappedBy = "account", fetch = FetchType.LAZY)
    private AccountDetail accountDetail;

    public static Builder Build() {
        return new Account().new Builder();
    }

    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return null;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }

    public class Builder {

        public Builder setUsername(String username) {
            Account.this.setUsername(username);
            return this;
        }

        public Builder setPassword(String password) {
            Account.this.setPassword(password);
            return this;
        }

        public Builder setPosition(String position) {
            Account.this.setPosition(position);
            return this;
        }

        public Builder setAccountType(AccountType accountType) {
            Account.this.setAccountType(accountType);
            return this;
        }

        public Builder setDepartment(DepartmentType departmentType) {
            Account.this.setDepartmentType(departmentType);
            return this;
        }

        public Builder setAccountDetail(AccountDetail accountDetail) {
            Account.this.setAccountDetail(accountDetail);
            return this;
        }

        public Account build() {
            return Account.this;
        }

    }

}