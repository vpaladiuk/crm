package com.cusbee.config.security;


public interface SecurityService {

    String findLoggedInUsername();

    void autoLogin(String username, String password);

}
